const express = require('express');
const app = express();

const path=require('path');
const home = require('./routes/home');
const chat = require('./routes/chat');
const dailyMeetings= require('./routes/dailyMeetings');
const itemsCreator = require('./routes/itemsCreator');
const itemsSynchronization = require('./routes/itemsSynchronization');
const jobScheduler = require('./routes/jobScheduler');
const managersDashboard = require('./routes/managersDashboard');
const notifications = require('./routes/notifications');
const other = require('./routes/other');
const setup = require('./routes/setup');


app.set('view engine', 'ejs');
app.set('views', path.join(__dirname,'/views'));
app.use(express.static('public'));

app.use('/home', home);
app.use('/chat', chat);
app.use('/dailymeetings', dailyMeetings);
app.use('/itemscreator', itemsCreator);
app.use('/itemssynchronization', itemsSynchronization);
app.use('/jobscheduler', jobScheduler);
app.use('/managersDashboard', managersDashboard);
app.use('/notifications', notifications);
app.use('/other', other);
app.use('/setup', setup);

app.listen(3000, () => {
 console.log("Listening on port 3000")
})
