const express= require('express');
const router= express.Router();

router.get('/', (req,res) =>{
    res.render('dailyMeetings.ejs');
})

module.exports = router;