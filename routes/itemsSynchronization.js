const express= require('express');
const router= express.Router();

router.get('/', (req,res) =>{
    res.render('itemsSynchronization.ejs');
})

module.exports = router;